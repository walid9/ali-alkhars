<?php

return [
    'our_services' => 'Our Services',
    'request_price_offer' => 'Request Price Offer',
    'other_services' => 'Other Services',
    'more' => 'Show more',
    'contact_us' => 'Contact us',
    'contact' => 'Contact us',
    'follow_us' => 'Follow us',
    'fill_contact_us_form' => 'Please fill in the required data to request a service or quote, and our specialized team will respond to you as soon as possible',
    'blogs' => 'Blog',
    'company' => [
        'title' => 'Ali Alkhars Public Accountants and Consultants',
        'slogan' => 'Accuracy and professionalism .. for sustainable financial solutions'
    ],
    'team_members' => 'Team Members',

    'job_application' => 'Job Application',
    'name' => 'Name',
    'name_is_required' => 'Please enter your name',
    'email' => 'Email',
    'email_is_required' => 'Please enter a valid email address',
    'job_title' => 'Job title',
    'job_title_is_required' => 'Please enter your job title',
    'phone' => 'Phone',
    'phone_is_required' => 'Please enter your phone number',
    'resume' => 'Resume',
    'request_details' => 'Request Details',
    'send' => 'Send',

    'request_received' => 'We have received your request and will contact you shortly',

    'request_service' => 'Request Service',
    'company_name' => 'Company Name',
    'select_service' => 'Select Service',

    'suggested_posts' => 'Suggested Posts'
];
