<?php

return [
    'our_services' => 'خدماتنا',
    'request_price_offer' => 'طلب عرض سعر',
    'other_services' => 'خدمات أخرى',
    'more' => 'رؤية المزيد',
    'contact_us' => 'اتصل بنا',
    'contact' => 'تواصل معنا',
    'follow_us' => 'تابعنا',
    'fill_contact_us_form' => 'من فضلك قم بملء البيانات المطلوبة لطلب خدمة أو عرض سعر وسيقوم فريقنا المختص بالرد عليكم في أقرب وقت ممكن',
    'blogs' => 'المدونة',
    'company' => [
        'title' => 'علي الخرس محاسبون قانونيون',
        'slogan' => 'دقة ومهنية .. لحلول مالية مستدامة'
    ],
    'team_members' => 'فريق العمل',

    'job_application' => 'طلب وظيفة',
    'name' => 'الاسم',
    'name_is_required' => 'من فضلك ادخل اسمك',
    'email' => 'البريد الالكتروني',
    'email_is_required' => 'من فضلك ادخل بريدك الالكتروني بشكل صحيح',
    'job_title' => 'المسمى الوظيفي',
    'job_title_is_required' => 'من فضلك ادخل المسمى الوظيفي الخاص بك',
    'phone' => 'رقم التواصل',
    'phone_is_required' => 'من فضلك ادخل رقم التواصل',
    'resume' => 'السيرة الذاتية',
    'request_details' => 'تفاصيل الطلب',
    'send' => 'إرسال',

    'request_received' => 'تم استقبال طلب سيادتكم وسيتم التواصل معكم في أقرب وقت',

    'request_service' => 'طلب خدمة',
    'company_name' => 'اسم المنشأة',
    'select_service' => 'اختر خدمة',

    'suggested_posts' => 'مقالات مقترحة'

];
