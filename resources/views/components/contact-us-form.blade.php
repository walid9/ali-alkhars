<section class="component-form">
    <div class="container">
        <div class="content-form">
            <div class="row mx-0">
                <div class="col-md-7 component-servers pt-5">
                    @if(Session::has('contacts_added_successfully'))
                        <div class="alert alert-success">
                            {{Session::get('contacts_added_successfully')}}
                        </div>
                    @endif
                    <div class="row justify-content-between mx-0 align-items-center">
                        <h6 class="col px-md-5 title-whit">{{__('website.request_service')}}</h6>
                        <div class="col col-5 px-md-5 f-img">
                            <img class="img-content" src="{{ mix('images/Icon-mail.svg') }}" alt="file">
                        </div>
                    </div>

                    <form enctype="multipart/form-data"
                          class="row gx-5 gy-md-5 gy-3 p-sm-4 mx-0 pt-3 needs-validation form-content-form form-content-as"
                          method="post" novalidate="" action="{{route('contact.store')}}">
                        @csrf
                        <div class="col-lg-6 col-md-12 px-3">
                            <input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror"
                                   placeholder="{{__('website.name')}}" id="validationCustom01" required
                                   name="name" value="{{ old('name') }}">
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.name_is_required')}}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 px-3">
                            <input type="email"
                                   class="form-control form-control-lg @error('email') is-invalid @enderror"
                                   placeholder="{{__('website.email')}}" id="validationCustom02" required
                                   name="email" value="{{ old('email') }}">
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.email_is_required')}}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 px-3">
                            <input type="text" class="form-control form-control-lg"
                                   placeholder="{{__('website.company_name')}}" id="validationCustom03"
                                   name="company_name" value="{{ old('company_name') }}">
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.name_is_required')}}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 px-3">
                            <input type="tel" class="form-control form-control-lg @error('phone') is-invalid @enderror"
                                   placeholder="{{__('website.phone')}}" id="validationCustom04" required
                                   name="phone" value="{{ old('phone') }}"
                                   style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                            @error('phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.phone_is_required')}}
                            </div>
                        </div>
                        <div class="col-md-12 px-3">
                            <select class="form-control form-select" id="validationCustom05" name="service_id">
                                <option selected disabled>{{__('website.select_service')}}</option>
                                @foreach($all_services as $service_item)
                                    <option value="{{$service_item->id}}">{{$service_item->title}}</option>
                                @endforeach
                            </select>
                            <div class="valid-feedback"></div>
                        </div>
                        <div class="col-md-12 px-3">
                            <textarea rows="5" class="form-control form-control-lg"
                                      placeholder="{{__('website.request_details')}}"
                                      name="body"></textarea>
                        </div>
                        <div class="col-12 text-center ">
                            <button class="btn btn-submit" type="submit">
                                <img src="{{ mix('images/Path.png') }}" alt="path">
                                {{__('website.send')}}
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-5 compoent-content-us component-servers pt-5 px-0 pb-4">
                    <div class="row   justify-content-between mx-0 align-items-center">
                        <h6 class="col px-md-5  title-whit">{{__('website.contact')}}</h6>
                        <div class="col col-5 px-md-5 f-img">
                            <img class="img-content-us" src="{{mix('images/Iconionic.svg') }}" alt="file">

                        </div>
                    </div>

                    <div class="pt-4  component-cont">
                        <div class="contact-contente link-contentphon-us  pt-3 pb-3">
                            <a href="tel:+966 50 603 9926" class="content-box px-md-5 px-3">
                                <i class="fa fa-phone"></i>
                               <span> {{setting('company.phone')}} </span>
                            </a>
                        </div>
                        <div class="contact-contente link-contentemail-us  pt-3 pb-3">
                            <a href="mailto:info@akcpa.co" class="content-box px-md-5 px-3">
                                <i class="fas fa-envelope"></i>
                                {{setting('company.email')}}
                            </a>
                        </div>
                    </div>
                    <!--nav-tap-->
                    <ul class="nav-tabs-content nav nav-tabs pt-4 justify-content-center" id="myTab" role="tablist">
                        @foreach($locations as $key => $location)
                            <li class="nav-item" role="presentation">
                                <button class="nav-link {{$key == 0 ? 'active' : ''}}" id="tab-{{$key}}"
                                        data-bs-toggle="tab"
                                        data-bs-target="#tab-pane-{{$key}}" type="button" role="tab"
                                        aria-controls="tab-pane-{{$key}}" aria-selected="true">{{$location->title}}
                                </button>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content tap-content-content px-5" id="myTabContent">
                        @foreach($locations as $key => $location)
                            <div class="tab-pane fade show {{$key == 0 ? 'active' : ''}}" id="tab-pane-{{$key}}"
                                 role="tabpanel"
                                 aria-labelledby="tab-{{$key}}" tabindex="{{$key}}">
                                <iframe
                                    src="{{$location->location_url}}"
                                    height="450" style="border:0;" allowfullscreen="" loading="lazy"
                                    referrerpolicy="no-referrer-when-downgrade"></iframe>
                            </div>
                        @endforeach
                    </div>
                    <!--end-nav-tap-->
                </div>
            </div>
        </div>
    </div>
</section>
