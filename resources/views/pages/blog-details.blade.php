@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <section class="component-content-header">
        <div class="content-img-header">
            <img class="img-haeder img-hader-blog" src="{{ Voyager::image($post->poster_image) }}"
                 alt="{{$post->title}}">
            <div class="img-logo-hader img-blog-hader">
                <div class="container">
                    <div class="row justify-content-between align-items-center">
                        <h6 class="col hade-title-blog">
                            {{$post->title}}
                        </h6>
                        <img class="col-sm-6" src="{{ mix('images/logo-hader.png') }}" alt="logo">
                    </div>
                </div>
            </div>
            <div id="over"></div>
        </div>
    </section>
    <!--content-hadeer-->
    <!--component-blog-->
    <section class="component-services pt-5 pb-5">
        <div class="container">
            <div class="img-lader-blog">
                <div class="row mx-0 justify-content-between pb-4">
                    <div class="col d-flex align-items-center">
                        <img class="img-leder" src="{{Voyager::image($post->authorId->avatar)}}" alt="img">
                        <div class="px-4">
                            <h6 class="title-leder">{{$post->authorId->name}}</h6>
                            <p class="p-bosition">{{$post->authorId->title}}</p>
                        </div>

                    </div>
                    <div class="col time-blog-detalies">
                        <div class="text-start">
                            {{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('d M') }}
                            <span>{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('Y') }}</span>
                        </div>
                    </div>
                </div>

            </div>
            <div class="defintion-blog pt-5">
               <pre class="pre-detiles">
                    {{$post->body}}
               </pre>
            </div>
            <!---->
            <section class="copmonent-Suggested-articles pt-5">
                <h5 class="title-footer">{{__('website.suggested_posts')}}</h5>
                <div class="owl-carousel owl-articles mt-5 ">
                    @foreach($suggested_posts as $post)
                        <div class="item ms-3 me-2">
                            <div class="col d-flex">
                                <div class="card card-blog">
                                    <a href="/blogs/{{$post->id}}">
                                        <div class="img-blog-date">
                                            <img class="blog-img" src="{{ Voyager::image($post->image) }}" alt="zaka">
                                            <div class="date-time">
                                                <div>{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('d') }}</div>
                                                <div>{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('M') }}</div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="title-card">{{$post->title}} </h6>
                                            <p class="title-p">
                                                {{$post->excerpt}}
                                            </p>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>

        </div>
    </section>
    <!--end-component-service-->
@endsection
