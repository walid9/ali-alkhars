@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <section class="component-content-header">
        <div class="content-img-header">
            <img class="img-haeder" src="{{ mix('images/hader.png') }}" alt="hader-img">
            <div class="img-logo-hader">
                <div class="container">
                    <img class="mt-4 mx-md-5" src="{{ mix('images/logo-hader.png') }}" alt="logo">
                </div>
            </div>
            <div id="over"></div>
        </div>
    </section>

    <section class="component-about pt-5 pb-5">
        <div class="container">
            <h5 class="title-footer-i">{{$page->title}}</h5>
            <p class="pt-4 mt-3 p-size">
                {{$page->excerpt}}
            </p>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="component-Features mt-5">
                @foreach($about_posts as $post)
                <div class="row-features row mx-0 pt-3">
                    <div class="col-lg-1 col-md-2 col-sm-6">
                        <img class="img-balance" src="{{ Voyager::image($post->image)}}" alt="balance">
                    </div>
                    <div class="defintion-features col-lg-10 col-md-10 col-sm-12 px-md-4">
                        <h6>{{$post->title}}</h6>
                        <ul class="pt-3">
                            @foreach($post->body as $detail)
                            <li>
                               {{$detail}}
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!--component-vision-->
    <section class="component-vision-about pt-5 pb-5">
        <div class="container">
            <div class="row mx-0">
                <div class="col-md-5 ps-5  col-img-vis">
                    <img class="img-vision" src="{{ Voyager::image($vision->image) }}" alt="vision">
                </div>
                <div class="col-md-6 pt-3">
                    <h6 class="col title-footer-i">{{$vision->title}}</h6>
                    <p class="pt-4 p-vision p-visin">
                        {{$vision->body}}
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--end-vision-->
    <!--mission.-->
    <section class="compoent-mission pt-5 pb-5">
        <div class="container">
            <div class="row mx-0">

                <div class="col-md-6">
                    <h6 class="col title-footer-i">{{$mission->title}}</h6>
                    <p class="pt-4 p-vision p-visin">
                        {{$mission->body}}
                    </p>
                </div>
                <div class="col-md-5 col-img-massege">
                    <img class="img-vision" src="{{ Voyager::image($mission->image) }}" alt="our">
                </div>
            </div>
        </div>
    </section>
    <!--end-mission.-->
    <!--hadeer-team-financial partner-->
    <section class="component-team-hadeer">
        <div class="img-team-hadeer">
            <img class="img-team-top w-100" src="{{ mix('images/team-hadeer.png') }}" alt="team">
            <div id="over-team"></div>
            <div class="content-team">
                <div class="container">
                    <div class="row mx-0 justify-content-between align-items-center">
                        <div class="col defintion-hadeer-team">
                            <h6>{{__('website.company.title')}}</h6>
                            <p class="pt-3">{{__('website.company.slogan')}}</p>
                        </div>
                        <img class="col-md-4 img-logo-team" src="{{ mix('images/logo-hader.png') }}" alt="logo">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end-financial partner-->
    <!---financial partner-->
    <section class="component-financial pt-5">
        <div class="container">
            <div class="table-top-table  ">
                @foreach($goals_slider->images as $slider_image)
                    <div class="i{{$slider_image->order}} {{$slider_image->order == 1 ? 'activ-link current-img' : ''}}"
                         id="img-op{{$slider_image->order}}">
                        <img class="img-financial" src="{{ Voyager::image($slider_image->image)  }}" alt=img>
                    </div>
                @endforeach
            </div>
            <div class="table-top pt-5 pb-5">
                <!---->
                @foreach($goals_slider->images as $slider_image)
                    <li class="b{{$slider_image->order}} col-4 col-button {{$slider_image->order == 1 ? 'activ-link activ-over' : ''}}"
                        onclick="changeSize('{{$slider_image->order}}', {{$slider_image->animation_order}} )"
                        data-cat=".all">
                        <button class="item-compaion">
                            <div
                                class="title-text-button">{{ strtok($slider_image->getTranslatedAttribute('excerpt', 'en'), " ") }}</div>
                            <div
                                class="text-button title-onr">{{$slider_image->getTranslatedAttribute('title', 'en')}}</div>
                            <div
                                class="text-button title-tow-com"> {{$slider_image->getTranslatedAttribute('title', 'ar')}}</div>
                        </button>
                    </li>
                @endforeach
            </div>
        </div>

    </section>
    <!--end-financial partner-->
    @if(isset($manager) || isset($other_members))
    <!--hadeer-team-financial partner-->
    <section class="component-team-hadeer">
        <div class="img-team-hadeer">
            <img class="img-team-top w-100" src="{{ mix('images/team-hadeer.png') }}" alt="team">
            <div id="over-team"></div>
            <div class="content-team">
                <div class="container">
                    <div class="row mx-0 justify-content-between align-items-center">
                        <div class="col defintion-hadeer-team">
                            <h6>{{__('website.company.title')}}</h6>
                            <p class="pt-3">{{__('website.team_members')}}</p>
                        </div>
                        <img class="col-md-4 img-logo-team" src="{{ mix('images/logo-hader.png') }}" alt="logo">

                    </div>

                </div>
            </div>


        </div>
    </section>
    <!--end-financial partner-->
    @endif
    <!--start-team-->
    
    <section class="component-team pt-5">
        <div class="container">
        @if(isset($manager))
            <!--Managing-partner-->
            <div class="content-Managing-partner">
                <div class="row mx-0 align-items-start">
                    <div class="col-md-4 px-0 pe-team-lader">
                        <img class="team-item" src="{{ Voyager::image($manager->image) }}" alt="partner">
                    </div>
                    <div class="defintion-team col-md-8 pt-5 px-lg-5">
                        <h4>{{$manager->name}}</h4>
                        <h6>{{$manager->title}}</h6>
                        <ul class="pt-4 px-3">
                            @foreach($manager->job_description  as $description)
                                <li>
                                    {{$description}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!--end-Managing-partner-->
            @endif
            <!--start-team-->
            <section class="component-team-item">
@if(isset($other_members))
                <div class="row row-cols-md-2 row-cols-1 g-4 mx-0 mt-5  row-team">
                @foreach($other_members  as $member)
                    <!--item--->
                        <div class="col d-flex col-roe-team mb-4">
                            <div class="content-Managing-partner px-lg-4 pt-4">
                                <div class="pe-img">
                                    <img class="team-item-team" src="{{ Voyager::image($member->image) }}" alt="partner">
                                </div>
                                <div class="defintion-team pt-3 pe-3">
                                    <h4 class="text-center">{{$member->name}}</h4>
                                    <h6 class="text-center">{{$member->title}}</h6>
                                    <ul class="pt-4 px-4">
                                    @if(is_array($member->job_description))
                                        @foreach($member->job_description  as $description)
                                            <li>
                                                {{$description}}
                                            </li>
                                        @endforeach
                                    @endif    
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--end-item-->
                    @endforeach
                </div>
                @endif
            </section>

            <!--end-team-->
        </div>
    </section>
    <!--end-team-->
@endsection
