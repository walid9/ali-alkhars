@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <!--content-hadeer-->
    <section class="component-content-header">
        <div class="content-img-header">
            <img class="img-haeder" src="{{ mix('images/hader.png') }}" alt="hader-img">
            <div class="img-logo-hader">
                <div class="container">
                    <img class="mx-md-5 mx-3 mt-md-4" src="{{ mix('images/logo-hader.png') }}" alt="logo">

                </div>
            </div>
            <div id="over"></div>
        </div>
    </section>
    <!--content-hadeer-->
    <!--component-services-->
    <section class="component-services pt-5 pb-5">
        <div class="container">
            <div class="component-content-services">
                <h5 class="title-hadeer title-hadeer-s mb-2 text-center">{{$page->title}}</h5>
            </div>
            <div class="row mx-0 row-cols-lg-3 row-cols-md-2 row-cols-1 gx-5 mt-4">
                @foreach($services as $service)
                @if($service->id !== 52)
                    <div class="col col-serves">
                        <div class="content-defintion  pt-4">
                            <span class="spanc h-100"></span>
                            <div class="defintion-defintion">
                                <div class="img-servies">
                                    <img src="{{ Voyager::image( $service->image ) }}" alt="search">
                                </div>
                                <h6 class="title-defintion text-center pt-3">{{$service->title}}</h6>
                                <ul class="ul-services pt-2 px-md-4 px-4 mb-3">
                                    @foreach($service->subServices as $subService)
                                    <li>{{$subService->title}}</li>
                                    @endforeach
                                </ul>
                                <div class="link-asking pt-2 pb-2 text-center">
                                    <a href="/price-offer-request">{{__('website.request_price_offer')}}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>
@endsection
