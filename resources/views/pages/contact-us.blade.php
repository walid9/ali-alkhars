@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <section class="component-content-header">
        <div class="content-img-header">
            <img class="img-haeder" src="{{ mix('images/hader.png') }}" alt="hader-img">
            <div class="img-logo-hader">
                <div class="container">
                    <img class="mx-md-5 mx-3 mt-md-4" src="{{ mix('images/logo-hader.png') }}" alt="logo">
                </div>
            </div>
            <div id="over"></div>
        </div>
    </section>

    <section class="component-hadeer-content-us">
        <div class="contents-img pt-5 pb-5">
            <div id="over-content"></div>
            <div class="defintion-content">
                <div class="container">
                    <h5 class="title-hadeer mb-2">{{$page->title}}</h5>
                    <p class="pt-4 mt-4 defintion-p col-md-12">{{$page->excerpt}}</p>
                </div>
            </div>
        </div>
    </section>
    <!--component-hadeer-content-us-->
    <!--form-content-->
    @include('components/contact-us-form')
@endsection
