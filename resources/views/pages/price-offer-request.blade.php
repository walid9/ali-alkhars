@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <!--content-hadeer-->
    <section class="component-content-header">
        <div class="content-img-header">
            <img class="img-haeder" src="{{ mix('images/hader.png') }}" alt="hader-img">
            <div class="img-logo-hader">
                <div class="container">
                    <img class=" mx-md-5 mx-3" src="{{ mix('images/logo-hader.png') }}" alt="logo">
                </div>
            </div>
            <div id="over"></div>
        </div>
    </section>
    <!--content-hadeer-->
    <!--form-asking-->
    <section class="component-form">
        <div class="container">
            <div class="content-form p-lg-5 p-md-4  mb-5 mx-md-5">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="row justify-content-between mx-0 align-items-center">
                    <h6 class="col col-7 px-md-5 title-whit">{{__('website.request_price_offer')}}</h6>
                    <div class="col col-5 px-md-5 f-img">
                        <img class="img-file" src="{{ mix('images/file.svg') }}" alt="file">
                    </div>
                </div>

                <form enctype="multipart/form-data"
                      class="row gx-5 gy-md-5 gy-3 p-sm-4 mx-0 pt-3 needs-validation form-content-form form-content-as"
                      method="post" novalidate="" action="{{route('priceOfferRequest.store')}}">
                    @csrf
                    <div class="col-lg-6 col-md-12 px-3">
                        <input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror"
                               placeholder="{{__('website.name')}}" id="validationCustom01" required
                               name="name" value="{{ old('name') }}">
                        @error('name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                        <div class="valid-feedback"></div>

                        <div class="invalid-feedback">
                            {{__('website.name_is_required')}}
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 px-3">
                        <input type="email"
                               class="form-control form-control-lg @error('email') is-invalid @enderror"
                               placeholder="{{__('website.email')}}" id="validationCustom02" required
                               name="email" value="{{ old('email') }}">
                        @error('email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                        <div class="valid-feedback"></div>

                        <div class="invalid-feedback">
                            {{__('website.email_is_required')}}
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 px-3">
                        <input type="text" class="form-control form-control-lg"
                               placeholder="{{__('website.company_name')}}" id="validationCustom03"
                               name="company_name" value="{{ old('company_name') }}">
                        <div class="valid-feedback"></div>
                    </div>
                    <div class="col-lg-6 col-md-12 px-3">
                        <input type="tel" class="form-control form-control-lg @error('phone') is-invalid @enderror"
                               placeholder="{{__('website.phone')}}" id="validationCustom04" required
                               name="phone" value="{{ old('phone') }}"
                               style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                        @error('phone')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                        <div class="valid-feedback"></div>

                        <div class="invalid-feedback">
                            {{__('website.phone_is_required')}}
                        </div>
                    </div>
                    <div class="col-md-12 px-3">
                        <select class="form-control form-select" id="validationCustom05" name="service_id">
                            <option selected disabled>{{__('website.select_service')}}</option>
                            @foreach($all_services as $service_item)
                                <option value="{{$service_item->id}}">{{$service_item->title}}</option>
                            @endforeach
                        </select>
                        <div class="valid-feedback"></div>
                    </div>
                    <div class="col-md-12 px-3">
                            <textarea rows="5" class="form-control form-control-lg"
                                      placeholder="{{__('website.request_details')}}"
                                      name="body"></textarea>
                    </div>
                    <div class="col-12 text-center ">
                        <button class="btn btn-submit" type="submit">
                            <img src="{{ mix('images/Path.png') }}" alt="path">
                            {{__('website.send')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
