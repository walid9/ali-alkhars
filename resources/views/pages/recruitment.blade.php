@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <!--slider-->
    <section class="component-content-header">
        <div class="content-img-header">
            <img class="img-haeder" src="{{ mix('images/hader.png') }}" alt="hader-img">
            <div class="img-logo-hader">
                <div class="container">
                    <img class="mt-4 mx-md-5" src="{{ mix('images/logo-hader.png') }}" alt="logo">

                </div>
            </div>
            <div id="over"></div>
        </div>
    </section>
    <!--content-hadeer-->
    <!--component-hadeer-content-us-->
    <section class="component-hadeer-content-us">
        <div class="contents-imgeer pt-1 pb-5">
            <div id="over-content"></div>

            <div class="defintion-content">
                <div class="container">
                    <div class="row mx-0 align-items-center mb-3">
                        <div class="col-md-8">
                            <h5 class="title-hadeer title-hadeer-jop mb-2 pt-5">{{$page->title}}</h5>
                            <p class="pt-3 defintion-p col-md-8">
                                {{$page->excerpt}}
                            </p>
                        </div>
                        <div class="col-md-4">
                            <img class="img-jop" src="{{ Voyager::image($page->image) }}" alt="jop">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--component-hadeer-content-us-->
    <!--form-content-->
    <section class="component-form">
        <div class="container">
            <div class="content-form p-lg-5 p-md-4 mb-5">
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                @endif
                <div class="component-servers">
                    <div class="row justify-content-between mx-0 align-items-center">
                        <h6 class="col px-md-5  title-whit">{{__('website.job_application')}}</h6>
                        <div class="col col-5 px-md-5 f-img">
                            <img class="img-jopee" src="{{ mix('images/Path-serves.svg') }}" alt="file">
                        </div>
                    </div>
                    <form enctype="multipart/form-data"
                          class="row gx-5 gy-md-5 gy-3 p-sm-4 mx-0 pt-3 needs-validation form-content-form form-content-as"
                          method="post" novalidate="" action="{{route('recruitment.store')}}">
                        @csrf

                        <div class="col-md-6">
                            <input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror"
                                   placeholder="{{__('website.name')}}" id="validationCustom01" required
                                   name="name" value="{{ old('name') }}">
                            @error('name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.name_is_required')}}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <input type="email"
                                   class="form-control form-control-lg @error('email') is-invalid @enderror"
                                   placeholder="{{__('website.email')}}" id="validationCustom02" required
                                   name="email" value="{{ old('email') }}">
                            @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.email_is_required')}}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <input type="text" class="form-control form-control-lg @error('job_title') is-invalid @enderror"
                                   placeholder="{{__('website.job_title')}}"
                                   id="validationCustom03" required
                                   name="job_title" value="{{ old('job_title') }}">
                            @error('job_title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.job_title_is_required')}}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <input type="tel" class="form-control form-control-lg @error('phone') is-invalid @enderror"
                                   placeholder="{{__('website.phone')}}" id="validationCustom04" required
                                   name="phone" value="{{ old('phone') }}"
                            style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                            @error('phone')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback">
                                {{__('website.phone_is_required')}}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="d-flex form-control form-control-lg custom-file justify-content-between">
                                <span>{{__('website.resume')}}</span>
                                <div class="">
                                    <input type="file" class="custom-file-input form-control-lg @error('resume') is-invalid @enderror" id="file" multiple=""
                                           onchange="updateList()" name="resume" value="{{ old('resume') }}">
                                    <label class="text-white custom-file-label" for="file">
                                        <img class="download" src="{{ mix('images/file-from.png') }}" alt="download">
                                    </label>
                                </div>
                            </div>
                            <ul id="fileList" class="file-list"></ul>
                            @error('resume')
                            <div class="invalid-feedback" style="display:block;">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>
                        </div>

                        <div class="col-md-12">
                            <textarea rows="5" class="form-control form-control-lg"
                                      placeholder="{{__('website.request_details')}}"
                                      name="body"></textarea>
                        </div>

                        <div class="col-12 text-center">
                            <button class="btn btn-submit" type="submit">
                                <img src="{{ mix('images/Path.png') }}" alt="path">
                                {{__('website.send')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
