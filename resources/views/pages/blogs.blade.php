@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <section class="component-services  pt-5 pb-5">
        <div class="container">
            <div class="row mx-0 row-cols-lg-3 row-cols-md-2 row-cols-1 g-4 mt-4">
            @foreach($posts as $post)
                <!--start-->
                    <div class="col d-flex">
                        <div class="card card-blog">
                            <a href="/blogs/{{$post->id}}">
                                <div class="img-blog-date">
                                    <img class="blog-img" src="{{ Voyager::image($post->image) }}" alt="zaka">
                                    <div class="date-time">
                                        <div>{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('d') }}</div>
                                        <div>{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('M') }}</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h6 class="title-card">{{$post->title}} </h6>
                                    <p class="title-p">
                                        {{$post->excerpt}}
                                    </p>
                                </div>
                            </a>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
