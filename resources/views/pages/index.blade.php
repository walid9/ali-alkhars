@extends('layouts.app')

@section('title', setting('site.title'))

@section('content')
    <!--slider-->
    <div id="carouselExampleCaptions" class="carousel slide carousel-slider" data-bs-ride="false">

        <div class="carousel-inner">
            @foreach($main_slider->images as $slider_image)
                <div class="carousel-item {{$main_slider->images[0]->id == $slider_image->id ? 'active' : ''}}">
                    <div class="img-slider">
                        <img src="{{ Voyager::image($slider_image->image) }}" class="img-slid d-block w-100"
                             alt="...">
                        <div class="over-slid"></div>
                        <img class="img-logo-slid" src="{{ mix('images/logo-ali.png') }}" alt="logo">
                    </div>
                    <div class="carousel-caption">
                        <h5 class="col-md-8">{{$slider_image->getTranslatedAttribute('title')}}</h5>
                        <p class="pt-4">{{$slider_image->getTranslatedAttribute('excerpt')}}</p>
                    </div>
                </div>
            @endforeach
        </div>
        <!--services-item-->
        <div class="item-services-slider mt-xl-4 mt-lg-4 mt-md-3">
            <div class="container">
                <div class="col-lg-6 col-md-9 m-auto">
                    <div class="row row-cols-lg-2 row-cols-md-2 row-cols-1 mx-0 gx-5  text-center">
                    
                        @foreach($home_services as $service_item)
                            <div class="col mb-3 col-servese">
                                <div class="img-top">
                                    <img src="{{ Voyager::image($service_item->logo) }}" alt="">
                                </div>
                                <div class="col-link-s">
                                    <a href="/services">
                                        <div class="col-defintion">
                                            {{$service_item->title}}
                                        </div>
                                        <span class="span-mor">{{__('website.more')}}</span>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="link-ether text-center">
                        <a href="/services">
                            <img src="{{ mix('images/self-service.png') }}" alt="self">
                            {{__('website.other_services')}}
                        </a>
                    </div>
                </div>

            </div>

        </div>
        <!--end-services-item-->
        <!--asking-price-hadder-->
        <div class="compoent-price-hadder">
            <div class="back">
                <div class="dropdown">

                    <button class="btn btn-secondary btn-btn d-flex mx-0 justify-content-between dropbtn" type="button"
                            onclick="myFunction()" id="myDIV">
                        <span class="title-botton">{{__('website.request_price_offer')}}</span>
                        <span class="span-i">
                                   <i class="fas fa-chevron-left"></i>
                              </span>
                    </button>
                </div>
                <div id="myDropdown" class="dropdown-content dropdown-menu  col-lg-3 col-md-6">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    <form enctype="multipart/form-data"
                          class="row g-3 mx-0 pt-4 px-3 needs-validation form-index-form pb-3"
                          method="post" novalidate="" action="{{route('priceOfferRequest.store')}}">
                        @csrf
                        <div class="col-md-12">
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                   placeholder="{{__('website.name')}}" id="validationCustom01" required
                                   name="name" value="{{ old('name') }}">
                            @error('name')
                            <div class="invalid-feedback"
                                 style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback"
                                 style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                                {{__('website.name_is_required')}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   placeholder="{{__('website.email')}}" id="validationCustom02" required
                                   name="email" value="{{ old('email') }}">
                            @error('email')
                            <div class="invalid-feedback"
                                 style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                                {{ $message }}
                            </div>
                            @enderror
                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback"
                                 style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                                {{__('website.email_is_required')}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="tel" class="form-control @error('phone') is-invalid @enderror"
                                   placeholder="{{__('website.phone')}}" id="validationCustom03" required
                                   name="phone" value="{{ old('phone') }}"
                                   style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                            @error('phone')
                            <div class="invalid-feedback"
                                 style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                                {{ $message }}
                            </div>
                            @enderror

                            <div class="valid-feedback"></div>

                            <div class="invalid-feedback"
                                 style="text-align: {{app()->currentLocale() == 'ar' ? 'right' : 'left'}}">
                                {{__('website.phone_is_required')}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="form-control"
                                   placeholder="{{__('website.company_name')}}" id="validationCustom05"
                                   name="company_name" value="{{ old('company_name') }}">
                            <div class="valid-feedback"></div>
                        </div>
                        <div class="col-md-12">
                            <select class="form-control form-select" id="validationCustom04" name="service_id">
                                <option selected disabled>{{__('website.select_service')}}</option>
                                @foreach($all_services->translate() as $service_item)
                                    <option value="{{$service_item->id}}"
                                            style="color:black;">{{$service_item->title}}</option>
                                @endforeach
                            </select>
                            <div class="valid-feedback"></div>
                        </div>
                        <div class="col-md-12">
                           <textarea rows="3" class="form-control"
                                     placeholder="{{__('website.request_details')}}"
                                     name="body"></textarea>
                        </div>
                        <div class="col-12 text-center">
                            <button class="btn btn-submit" type="submit">
                                <img src="{{ mix('images/Path.png') }}" alt="path">
                                {{__('website.send')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <!--asking-price-hadder-->
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    <!--end-slider-->

    <!--our-vision-->
    <section class="component-vision pt-5 mt-5 pb-4">
        <div class="container">
            <div class="row row-cols-lg-2 row-cols-md-2 row-cols-1 mx-0 g-5">
                <div class="col col-vis">
                    <div class="card card-vision">
                        <img class="img-vision" src="{{ Voyager::image($vision->image) }}" alt="vision">
                        <div class="card-body">
                            <div class="row justify-content-between">
                                <h6 class="col title-footer-i">{{$vision->title}}</h6>
{{--                                <a class="col link-plas" href="">{{__('website.more')}}</a>--}}
                            </div>
                            <p class="pt-4 mt-3 p-vision">
                                {{$vision->body}}
                            </p>
                        </div>
                    </div>
                </div>
                <!--item-tow-->
                <div class="col col-vis">
                    <div class="card card-vision">
                        <img class="img-vision" src="{{ Voyager::image($mission->image) }}" alt="vision">
                        <div class="card-body">
                            <div class="row justify-content-between">
                                <h6 class="col title-footer-i">{{$mission->title}}</h6>
{{--                                <a class="col link-plas" href="">{{__('website.more')}}</a>--}}
                            </div>

                            <p class="pt-4 mt-3 p-vision">
                                {{$mission->body}}
                            </p>

                        </div>
                    </div>
                </div>
                <!--end-item-tow-->
            </div>
        </div>
    </section>
    <!--end-our vision-->
    <!--content-us-->
    <!--component-hadeer-content-us-->
    <section class="component-hadeer-content-us">
        <div class="contents-img pt-5 pb-5">
            <div id="over-content"></div>
            <div class="defintion-content">
                <div class="container">
                    <h5 class="title-hadeer mb-2">{{__('website.contact_us')}}</h5>
                    <p class="pt-3 defintion-p col-md-12">
                        {{__('website.fill_contact_us_form')}}
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!--component-hadeer-content-us-->
    <!--form-content-->
    @include('components/contact-us-form')
    <!--end-form-content-->
    <section class="copmonent-Suggested-articles pt-4 pb-5">
        <div class="container">
            <h5 class="title-footer">{{__('website.blogs')}}</h5>
            <div class="content-slider-articles col-lg-9">
                <div class="owl-carousel owl-articles mt-5 ">
                    @foreach($blog_posts as $post)
                        <div class="item ms-3 me-3 mb-3 mt-3">
                            <div class="col d-flex">
                                <div class="card card-blog">
                                    <a href="Blog-details.html">
                                        <div class="img-blog-date">
                                            <img class="blog-img" src="{{ Voyager::image($post->image) }}"
                                                 alt="zaka">
                                            <div class="date-time">
                                                <div>{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('d') }}</div>
                                                <div>{{ \Carbon\Carbon::parse($post->created_at)->translatedFormat('M') }}</div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <h6 class="title-card">{{$post->title}} </h6>
                                            <p class="title-p">
                                                {{$post->excerpt}}
                                            </p>
                                        </div>
                                    </a>

                                </div>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>

        </div>

    </section>
@endsection
