<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="Ali Al-Khars ,Chartered Accountants, Consultants">
    <meta name="description" content="Ali Al-Khars ,Chartered Accountants, Consultants">
    <meta name="author" content="hadeer hussein">
    <meta property="og:title" content="Ali Al-Khars"/>
    <meta property="og:type" content="Website"/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content="{{ mix('images/logo -a-k.png') }}"/>

    <link href="{{mix('images/logo -a-k.png')}}"  rel="shortcut icon"/>
    <link href="{{mix('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{mix('css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{mix('css/owl.theme.default.min.css')}}" rel="stylesheet">
    <link href="{{mix('css/style.css')}}" rel="stylesheet">
    <link href="{{mix('font-awesome/css/all.css')}}" rel="stylesheet">
    <link href="{{mix('css/style-en.css')}}" rel="stylesheet">

    <title>@yield('title')</title>
</head>
