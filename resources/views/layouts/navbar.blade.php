<div class="nav-top">
    <div class="container">
        <div class="row mx-0 row-cols-lg-2 row-cols-1 pb-2 align-items-center">
            <div class="col">
                <img class="logo-hader" src="{{Voyager::image(setting('site.logo'))}}" alt="logo">
            </div>
            <div class="col col-contact-hader ">
                <div class="row-nav-content row align-items-center  row-cols-1">
                    <div class="col-md-8 component-footer-content colemail justify-content-center mt-2">
                        <div class="contact-content mb-2">
                            <a href="mailto:{{setting('company.email')}}" class="content-box">
                                <i class="fas fa-envelope"></i>
                                {{setting('company.email')}}
                            </a>
                        </div>
                        <div class="contact-content mb-2">
                            <a href="tel:{{setting('company.phone')}}" class="content-box">
                                <i class="fa fa-phone"></i>
                                <span>{{setting('company.phone')}}</span>
                            </a>
                        </div>
                        <div class="contact-content mb-2">
                            <a class="content-box">
                                <i class="fa fa-location-dot"></i>
                                @foreach($locations as $location)
                                    {{$location->title}}
                                    @if (!$loop->last)
                                        -
                                    @endif
                                @endforeach
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 link-lengeth text-start">
                        <a href="/language/{{$other_locale['locale']}}">
                            {{$other_locale['locale_name']}}
                            <i class="fa-solid fa-globe"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--nav-->
<nav class="navbar navbar-expand-xl navber-index sticky-top">
    <div class="container">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            @php
                $items = menu('website', '_json');
            @endphp
            <ul class="navbar-nav m-auto mb-2 mb-lg-0 align-items-center">
                @foreach($items as $menu_item)
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{$menu_item->route}}">{{$menu_item->getTranslatedAttribute('title')}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>
<!--end-nav-->
