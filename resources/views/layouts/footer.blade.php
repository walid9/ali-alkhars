<footer>
    <div class="component-footer">
        <div class="container">
            <div class="row mx-0  pt-5 pb-3">
                <div class="col-md-8 component-serves">
                    <h5 class="title-footer">{{__('website.our_services')}}</h5>
                    <ul class="ul-servies pt-4">
                        @foreach($parent_services as $service_item)
                            <li class="ul-li-item mb-2">
                                <a href="/services/{{$service_item->slug}}">{{$service_item->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-4 component-content">
                    <div class="img-footer">
                        <img src="{{mix('images/logo.png') }}" alt="logo">
                    </div>
                    <!--start-social--->
                    <div class="social-footer col-lg-7 text-center mt-5">
                        <h6 class="title-follow">{{__('website.follow_us')}}</h6>
                        <div class="Social-Media d-flex justify-content-between mt-2">
                            <div class="item-social">
                                <a class="fab fa-linkedin" href="{{setting('company.linkedin')}}" target="_blank"></a>

                            </div>

                            <div class="item-social">
                                <a class="fab fa-twitter" href="{{setting('company.twitter')}}" target="_blank"></a>

                            </div>
                            <div class="item-social">
                                <a class="fab fa-facebook-f" href="{{setting('company.facebook')}}" target="_blank"></a>

                            </div>
                        </div>
                    </div>
                    <!--end-social-footer-->
                    <!--start-content-->
                    <div class="component-footer-content col-lg-9 col-md-12 m-auto justify-content-center mt-5">
                        <div class="contact-content mb-2">
                            <a href="mailto:{{setting('company.email')}}" class="content-box">
                                <i class="fas fa-envelope"></i>
                                {{setting('company.email')}}
                            </a>
                        </div>
                        <div class="contact-content mb-2">
                            <a href="tel:{{setting('company.phone')}}" class="content-box">
                                <i class="fa fa-phone"></i>
                                <span>{{setting('company.phone')}}</span>
                            </a>
                        </div>
                        <div class="contact-content mb-2">
                            <a class="content-box">
                                <i class="fa fa-location-dot"></i>
                                @foreach($locations as $location)
                                    {{$location->title}}
                                    @if (!$loop->last)
                                        -
                                    @endif
                                @endforeach
                            </a>
                        </div>
                    </div>
                    <!--end-content-->
                </div>
            </div>
        </div>
    </div>
    <div class="component-copy text-center pt-2 pb-2">
        &#169 2023 All rights reserved for RightClick
    </div>
</footer>

<script>
    var arrowLeft = '{{ mix('images/Icon-left-r.png') }}';
    var arrowRight = '{{ mix('images/arrow-right.png') }}';
</script>

<script src="{{mix('js/jquery.js')}}" rel="stylesheet"></script>
<script src="{{mix('js/owl.carousel.min.js')}}"></script>
<script src="{{mix('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{mix('js/form.js')}}"></script>

{{--@vite([--}}
{{--'resources/assets/js/jquery.js',--}}
{{--'resources/assets/js/owl.carousel.min.js',--}}
{{--'resources/assets/js/bootstrap.bundle.min.js',--}}
{{--'resources/assets/js/form.js'--}}
{{--])--}}

