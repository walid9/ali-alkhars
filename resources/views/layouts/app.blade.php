<!DOCTYPE html>
<html dir="{{(App::isLocale('ar') ? 'rtl' : 'ltr')}}">
@include('layouts/header')
<body>
@include('layouts/navbar')

@yield('content')

@include('layouts/footer')
</body>
</html>
