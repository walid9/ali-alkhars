<?php

use App\Http\Controllers\Website\BlogsController;
use App\Http\Controllers\Website\ContactsController;
use App\Http\Controllers\Website\PagesController;
use App\Http\Controllers\Website\PriceOfferRequestsController;
use App\Http\Controllers\Website\RecruitmentsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::group(['middleware' => ['localization']], function () {

    Route::get('/', [PagesController::class, 'home']);

    Route::get('/about', [PagesController::class, 'about']);

    Route::get('/blogs', [BlogsController::class, 'index']);
    Route::get('/blogs/{post}', [BlogsController::class, 'show']);


    Route::get('/contact-us', [ContactsController::class, 'index']);
    Route::post('/contact-us', [ContactsController::class, 'store'])->name('contact.store');


    Route::get('/services', [PagesController::class, 'services']);

    Route::get('/recruitment', [RecruitmentsController::class, 'index']);
    Route::post('/recruitment', [RecruitmentsController::class, 'store'])->name('recruitment.store');;

    Route::get('/price-offer-request', [PriceOfferRequestsController::class, 'index']);
    Route::post('/price-offer-request', [PriceOfferRequestsController::class, 'store'])->name('priceOfferRequest.store');

    Route::get('/language/{locale}', function ($locale) {
        app()->setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    });
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
