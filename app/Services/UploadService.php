<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class UploadService
{
    public function upload($file, $path)
    {
        $path = $path . '/' . date('FY') . '/';

        $filename = time() . '_' . $file->getClientOriginalName();

        $file->storeAs(
            $path,
            $filename,
            config('voyager.storage.disk', 'public')
        );

        return [
            'path' => $path,
            'filename' => $filename,
            'url' => Storage::disk(config('voyager.storage.disk', 'public'))->url($path . $filename)
        ];
    }
}
