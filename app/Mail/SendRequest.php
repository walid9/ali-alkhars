<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendRequest extends Mailable
{
    use Queueable, SerializesModels;

public $name;
public $email;
public $company_name;
public $phone;
public $service_id;
public $body;
public $request_type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email , $company_name , $phone , $service_id , $body , $request_type)
    {
        $this->name = $name;
        $this->email = $email;
        $this->company_name = $company_name;
        $this->phone = $phone;
        $this->service_id = $service_id;
        $this->body = $body;
        $this->request_type = $request_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->name;
        $email = $this->email;
        $company_name = $this->company_name;
        $phone = $this->phone;
        $service_id = $this->service_id;
        $body = $this->body;
        $request_type = $this->request_type;

        if ($request_type == 'Contact us')
        {
            return $this->view('emails.send_contact_request', compact('name', 'email' , 'company_name' , 'phone' , 'service_id' , 'body'))->subject('Ali alkhars - '.$request_type);

        }

        if ($request_type == 'Price Offer Request')
        {
            return $this->view('emails.send_price_request', compact('name', 'email' , 'company_name' , 'phone' , 'service_id' , 'body'))->subject('Ali alkhars - '.$request_type);

        }
    }
}
