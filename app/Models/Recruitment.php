<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Recruitment extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'job_title', 'body', 'resume'];
}
