<?php

namespace App\Models;
use TCG\Voyager\Traits\Translatable;


class Post extends \TCG\Voyager\Models\Post
{
    use Translatable;

    protected $translatable = ['title', 'body'];
    
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
