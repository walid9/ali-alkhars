<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Contact extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'company_name', 'service_id', 'body'];

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
