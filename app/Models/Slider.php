<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['page_id', 'title'];

    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id');
    }

    public function images()
    {
        return $this->hasMany(SliderImage::class,);
    }
}
