<?php

namespace App\Models;

use TCG\Voyager\Traits\Translatable;


class Page extends \TCG\Voyager\Models\Page
{
    use Translatable;

    protected $translatable = ['title', 'excerpt', 'slug', 'body'];

    public function sliders()
    {
        return $this->hasMany(Slider::class);
    }
}
