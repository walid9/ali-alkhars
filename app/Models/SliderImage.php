<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class SliderImage extends Model
{
    use Translatable;

    protected $table = 'slider_images';

    protected $translatable = ['title', 'excerpt'];

    public function slider()
    {
        return $this->belongsTo(Slider::class, 'slider_id');
    }
}
