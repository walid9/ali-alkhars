<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PriceOfferRequest extends Model
{
    protected $table = 'price_offers_requests';

    protected $fillable = ['name', 'email', 'phone', 'company_name', 'service_id', 'body'];

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
