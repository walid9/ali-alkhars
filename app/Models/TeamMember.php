<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class TeamMember extends Model
{
    use Translatable;

    protected $translatable = ['name', 'title', 'job_description'];

}
