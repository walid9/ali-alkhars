<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;
use App\Models\Service;
use App\Models\Slider;
use App\Models\TeamMember;
use App\Models\Post;
use function view;

class PagesController extends Controller
{
    public function home()
    {
        $posts = Post::whereStatus('PUBLISHED')
            ->with('category')
            ->get()
            ->translate();   

        $vision = $posts->where('slug', 'vision')->first();
        $mission = $posts->where('slug', 'mission')->first();


        $blogsCategoriesIds = Category::where('slug', 'blogs')
            ->first()
            ->descendantsAndSelf
            ->pluck('id')
            ->toArray();

        $blog_posts = $posts->whereIn('category.id', $blogsCategoriesIds);

        $sliders = Slider::with([
            'images' => function ($query) {
                return $query->orderBy('order')
                    ->withTranslations();
            }])
            ->whereIn('title', ['main'])
            ->get();

        $main_slider = $sliders->where('title', 'main')->first();

        return view('pages.index', compact(['main_slider', 'vision', 'mission', 'blog_posts']));
    }

    public function about()
    {
        $page = Page::whereSlug('about')
            ->first()
            ->translate();

        $posts = Post::whereStatus('PUBLISHED')
            ->whereIn('slug', ['vision', 'mission'])
            ->with('category')
            ->orWhereHas('category', function ($q) {
                $q->where('slug', 'about');
            })
            ->get();

        $vision = $posts->where('slug', 'vision')->first()->translate();
        $mission = $posts->where('slug', 'mission')->first()->translate();

        $about_posts = $posts->where('category.slug', 'about')->translate();

        foreach ($about_posts as $post) {
            $post->body = str_replace("\n", "<br>", $post->body);
            $post->body = explode("<br>", $post->body);
        }

        $sliders = Slider::with([
            'images' => function ($query) {
                return $query->orderBy('order')
                    ->withTranslations();
            }])
            ->whereIn('title', ['goals'])
            ->get();

        $goals_slider = $sliders->where('title', 'goals')->first();

        $teams_members = TeamMember::where('active' , 1)->get()->translate();
$other_members = $teams_members->where('order', '!=', 1);
        $manager = $teams_members->where('order', 1)->first();
        if(isset($manager))
        {

        $manager->job_description = str_replace("\n", "<br>", $manager->job_description);
        $manager->job_description = explode("<br>", $manager->job_description);

        foreach ($other_members as $member) {
            $member->job_description = str_replace("\n", "<br>", $member->job_description);
            $member->job_description = explode("<br>", $member->job_description);
        }
        }

        return view('pages.about', compact(
            'page',
            'about_posts', 'vision', 'mission',
            'goals_slider',
            'manager', 'other_members'));
    }

    public function services()
    {
        $services = Service::whereNull('parent_id')
            ->with('subServices')
            ->get()
            ->translate();

        $page = Page::whereSlug('services')->first()->translate();

        return view('pages.services', compact(['services', 'page']));
    }

}
