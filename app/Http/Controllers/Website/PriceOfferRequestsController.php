<?php

namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Mail\SendRequest;
use App\Models\Contact;
use App\Models\Page;
use App\Models\PriceOfferRequest;
use App\Models\Recruitment;
use App\Services\UploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use function view;

class PriceOfferRequestsController extends Controller
{

    public function index()
    {
        $page = Page::whereSlug('price-offer-request')
            ->first()
            ->translate();

        return view('pages.price-offer-request', compact('page'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);

        PriceOfferRequest::create($request->only(['name', 'email', 'company_name', 'phone', 'service_id', 'body']));

        Mail::to('client.relations@akcpa.co')->send(new SendRequest($request['name'], $request['email'] , $request['company_name'] , $request['phone'] , $request['service_id'] , $request['body'] , 'Price Offer Request'));

        return back()->with('success', __('website.request_received'));
    }

}
