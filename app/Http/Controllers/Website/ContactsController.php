<?php

namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Mail\SendOtp;
use App\Mail\SendRequest;
use App\Models\Contact;
use App\Models\Page;
use App\Models\Recruitment;
use App\Services\UploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use function view;

class ContactsController extends Controller
{

    public function index()
    {
        $page = Page::whereSlug('contact-us')
            ->first()
            ->translate();

        return view('pages.contact-us', compact('page'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
        ]);

        Contact::create($request->only(['name', 'email', 'company_name', 'phone', 'service_id', 'body']));

        Mail::to('client.relations@akcpa.co')->send(new SendRequest($request['name'], $request['email'] , $request['company_name'] , $request['phone'] , $request['service_id'] , $request['body'] , 'Contact us'));

        return back()->with('contacts_added_successfully', __('website.request_received'));
    }

}
