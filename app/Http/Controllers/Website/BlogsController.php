<?php

namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Page;
use App\Models\Post;
use App\Models\Recruitment;
use App\Services\UploadService;
use Illuminate\Http\Request;
use function view;

class BlogsController extends Controller
{

    public function index()
    {
        $blogsCategoriesIds = Category::where('slug', 'blogs')
            ->first()
            ->descendantsAndSelf
            ->pluck('id')
            ->toArray();

        $posts = Post::whereHas('category', function ($q) use ($blogsCategoriesIds) {
            $q->whereIn('id', $blogsCategoriesIds);
        })
            ->published()
            ->get()
            ->translate();

        return view('pages.blogs', compact('posts'));
    }

    public function show(Post $post)
    {
        $post = $post->translate();

        $suggested_posts = Post::whereNot('id', $post->id)
            ->whereHas('category', function ($q) use ($post) {
                $q->where('id', $post->category_id);
            })
            ->published()
            ->get()
            ->translate();

        return view('pages.blog-details', compact('post', 'suggested_posts'));
    }

}
