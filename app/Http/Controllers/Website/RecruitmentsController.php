<?php

namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Recruitment;
use App\Services\UploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use function view;

class RecruitmentsController extends Controller
{

    public function index()
    {
        $page = Page::whereSlug('recruitment')
            ->first()
            ->translate();

        return view('pages.recruitment', compact('page'));
    }

    public function store(Request $request, UploadService $uploadService)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'job_title' => 'required',
            'resume' => 'file|mimes:doc,docx,pdf|max:204800'
        ]);

        $file = $request->resume;

        if ($request->file())
            $uploadResume = $uploadService->upload($file, 'recruitment');

        $rec = new Recruitment([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'job_title' => $request->job_title,
            'resume' => $uploadResume['url'] ?? null,
            'body' => $request->body
        ]);

        $rec->save();
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['job_title'] = $request->job_title;
            $data['body'] = $request->body;
       
        $uploadFile = public_path('storage/recruitment/' . date('FY') . '/'.$uploadResume['filename']);
       
        Mail::send('emails.send_job_request', $data, function($message)use($data, $uploadFile) {
            $message->to('client.relations@akcpa.co')
                ->subject('Recruitment requests');
            $message->attach($uploadFile);
        });
        return back()->with('success', __('website.request_received'));
    }
}
