<?php

namespace App\Providers;

use App\Models\Location;
use App\Models\Page;
use App\Models\Post;
use App\Models\Service;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Voyager::useModel('Page', Page::class);
        Voyager::useModel('Post', Post::class);

        View::composer(['*pages.*'], function ($view) {

            $locations = Location::orderBy('order')
                ->get();

            $locations = $locations?->translate();

            $all_services = Service::orderBy('order')->get();

            if ($all_services) {
                $home_services = $all_services->whereNull('parent_id')->take(4)
                    ->translate();

                $parent_services = $all_services->whereNull('parent_id')
                    ->translate();
            }

            $available_locales = config('app.available_locales');
            $current_locale = config('app.locale');

            $other_locales = array_filter($available_locales, function ($locale_name) use ($current_locale) {
                return $locale_name !== $current_locale;
            }, ARRAY_FILTER_USE_BOTH);

            $view->with([
                'locations' => $locations,
                'other_locale' => [
                    'locale' => reset($other_locales),
                    'locale_name' => key($other_locales)
                ],
                'all_services' => $all_services ?? null,
                'home_services' => $home_services ?? null,
                'parent_services' => $parent_services ?? null
            ]);
        });

    }
}
